from keras.datasets import fashion_mnist
from keras.models import load_model
from transfer_model import TransferModel
import numpy as np

def main():
    results = {}
    '''
    print('transfer_baseline')
    transfer_baseline = TransferModel([0,2,4,5,6,7,8,9], [])
    results['transfer_baseline'] = transfer_baseline.fit('transfer_baseline')

    print('trouser_dress_baseline')
    trouser_dress_baseline = TransferModel([1,3], [])
    results['trouser_dress_baseline'] = trouser_dress_baseline.fit('trouser_dress_baseline')
    '''

    transfer_baseline = load_model('h5models/transfer_baseline_take2.h5')
    print('getting_weights')
    transfer_layer_1 = transfer_baseline.get_layer('conv1').get_weights()
    #print('transfer_layer_1: {}'.format(transfer_layer_1))
    #transfer_layer_2 = transfer_baseline.get_layer('conv2').get_weights()
    #print('transfer_layer_2: {}'.format(transfer_layer_2))
    #transfer_layer_3 = transfer_baseline.get_layer('conv3').get_weights()
    #print('transfer_layer_3: {}'.format(transfer_layer_3))

    print('transfer_1')
    transfer_1 = TransferModel([1,3], [transfer_layer_1])
    results['transfer_1'] = transfer_1.fit('transfer_1')
    '''
    print('transfer_2')
    transfer_2 = TransferModel([1,3], [transfer_layer_1, transfer_layer_2])
    results['transfer_2'] = transfer_2.fit('transfer_2')

    print('transfer_3')
    transfer_3 = TransferModel([1,3], [transfer_layer_1, transfer_layer_2, transfer_layer_3])
    results['transfer_3'] = transfer_3.fit('transfer_3')

    return results
    '''
    return True

if __name__ == '__main__':
    main()
