from keras.datasets import fashion_mnist
from transfer_model import TransferModel
import numpy as np

def main():
    (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

    trouser_indices_train = y_train == 1
    trouser_indices_test = y_test == 1
    dress_indices_train = y_train == 3
    dress_indices_test = y_test == 3
    rest_indices_train = np.logical_not(np.logical_or(trouser_indices_train, dress_indices_train))
    rest_indices_test = np.logical_not(np.logical_or(trouser_indices_test, dress_indices_test))

    results = {}

    print('transfer_baseline')
    transfer_baseline = TransferModel([0,2,4,5,6,7,8,9], [])
    results['transfer_baseline'] = transfer_baseline.fit('transfer_baseline')

    print('trouser_dress_baseline')
    trouser_dress_baseline = TransferModel([1,3], [])
    results['trouser_dress_baseline'] = trouser_dress_baseline.fit('trouser_dress_baseline')

    print('getting_weights')
    transfer_layer_1 = transfer_baseline.model.get_layer('conv1').get_weights()
    #print('transfer_layer_1: {}'.format(transfer_layer_1))
    transfer_layer_2 = transfer_baseline.model.get_layer('conv2').get_weights()
    #print('transfer_layer_2: {}'.format(transfer_layer_2))
    transfer_layer_3 = transfer_baseline.model.get_layer('conv3').get_weights()
    #print('transfer_layer_3: {}'.format(transfer_layer_3))
    transfer_layer_4 = transfer_baseline.model.get_layer('conv4').get_weights()
    #print('transfer_layer_3: {}'.format(transfer_layer_3))

    print('transfer_1')
    transfer_1 = TransferModel([1,3], [transfer_layer_1])
    results['transfer_1'] = transfer_1.fit('transfer_1')

    print('transfer_2')
    transfer_2 = TransferModel([1,3], [transfer_layer_1, transfer_layer_2])
    results['transfer_2'] = transfer_2.fit('transfer_2')

    print('transfer_3')
    transfer_3 = TransferModel([1,3], [transfer_layer_1, transfer_layer_2, transfer_layer_3])
    results['transfer_3'] = transfer_3.fit('transfer_3')

    print('transfer_4')
    transfer_4 = TransferModel([1,3], [transfer_layer_1, transfer_layer_2, transfer_layer_3, transfer_layer_4])
    results['transfer_4'] = transfer_4.fit('transfer_4')

    return results

if __name__ == '__main__':
    main()
