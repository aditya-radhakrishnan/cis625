"""TransferModel class"""
import keras
import numpy as np
import tensorflow as tf
from keras.datasets import fashion_mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.callbacks import CSVLogger, EarlyStopping
from keras.optimizers import Adam
from time import time

class TransferModel():
    def __init__(self, labels, init_weights):
        self.label_string = ''.join(str(num) for num in labels)
        label_map = {labels[i]: i for i in range(len(labels))}
        (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()
        '''
        x_train = x_train[:1000]
        y_train = y_train[:1000]
        x_test = x_test[:1000]
        y_test = y_test[:1000]
        '''
        x_train = np.expand_dims(x_train, axis=3)
        x_test = np.expand_dims(x_test, axis=3)
        train_mask = np.array([y_train[i] in labels for i in range(len(y_train))])
        test_mask = np.array([y_test[i] in labels for i in range(len(y_test))])
        self.x_train = x_train[train_mask]
        y_train = y_train[train_mask]
        if len(init_weights) > 0:
            samples = {
                1: 5900,
                2: 4700,
                3: 3500,
                4: 2300
            }
            self.x_train = self.x_train[:samples.get(len(init_weights), 'Invalid len(init_weights)')]
            y_train = y_train[:samples.get(len(init_weights), 'Invalid len(init_weights')]
            print('Number of Label 1 Samples: ', (y_train == 1).sum())
            print('Number of Label 3 Samples: ', (y_train == 3).sum())
        self.x_test = x_test[test_mask]
        y_test = y_test[test_mask]
        y_train = np.vectorize(lambda x: label_map[x])(y_train)
        y_test = np.vectorize(lambda x: label_map[x])(y_test)
        self.y_train = keras.utils.to_categorical(y_train, num_classes=len(labels))
        self.y_test = keras.utils.to_categorical(y_test, num_classes=len(labels))
        # input: 28x28 images with 1 channel -> (28, 28, 1) tensors.
        # this applies 32 convolution filters of size 3x3 each.
        self.model = Sequential()

        layer = Conv2D(16, (3, 3), activation='relu', input_shape=(28, 28, 1), name='conv1')
        if len(init_weights) > 0:
            layer = Conv2D(16, (3, 3), activation='relu', trainable=False, weights=init_weights[0], input_shape=(28, 28, 1), name='conv1')
        self.model.add(layer)

        layer = Conv2D(16, (3, 3), activation='relu', name='conv2')
        if len(init_weights) > 1:
            layer = Conv2D(16, (3, 3), activation='relu', trainable=False, weights=init_weights[1], name='conv2')
        self.model.add(layer)
        self.model.add(MaxPooling2D(pool_size=(2, 2), name='pool1'))
        self.model.add(Dropout(0.25, name='dropout1'))

        layer = Conv2D(16, (3, 3), activation='relu', name='conv3')
        if len(init_weights) > 2:
            layer = Conv2D(16, (3, 3), activation='relu', trainable=False, weights=init_weights[2], name='conv3')
        self.model.add(layer)
        self.model.add(MaxPooling2D(pool_size=(2, 2), name='pool2'))
        self.model.add(Dropout(0.25, name='dropout2'))

        layer = Conv2D(16, (3, 3), activation='relu', name='conv4')
        if len(init_weights) > 3:
            layer = Conv2D(16, (3, 3), activation='relu', trainable=False, weights=init_weights[2], name='conv4')
        self.model.add(layer)
        self.model.add(MaxPooling2D(pool_size=(2, 2), name='pool3'))
        self.model.add(Dropout(0.25, name='dropout3'))
        
        self.model.add(Flatten(name='flatten'))
        layer = Dense(64, activation='relu', name='fc1')
        if len(init_weights) > 4:
            layer = Dense(64, activation='relu', trainable=False, weights=init_weights[3], name='fc1')
        self.model.add(layer)
        self.model.add(Dropout(0.5, name='dropout4'))

        layer = Dense(64, activation='relu', name='fc2')
        if len(init_weights) > 5:
            layer = Dense(64, activation='relu', trainable=False, weights=init_weights[4], name='fc2')
        self.model.add(layer)
        self.model.add(Dropout(0.5, name='dropout5'))

        self.model.add(Dense(len(labels), activation='softmax', name='fcout'))
        print(self.model.summary())

    def fit(self, fname):
        opt = Adam()
        self.model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

        csv_logger = CSVLogger(fname + '_take2')
        early_stopping = EarlyStopping(patience=2)
        self.model.fit(self.x_train, self.y_train, batch_size=32, epochs=100, callbacks=[csv_logger, early_stopping])
        score = self.model.evaluate(self.x_test, self.y_test, batch_size=32)
        self.model.save('h5models/' + fname + '_take2.h5')
        return score
